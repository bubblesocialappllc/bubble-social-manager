//
//  BubbleSocialManager-Bridging-Header.h
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/24/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//
//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/BFTask.h>