//
//  BSUser.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse

class BSUser: PFUser, PFSubclassing {
    
    @NSManaged var displayName: String
    @NSManaged var staff: Boolean
    @NSManaged var beta: Boolean
    @NSManaged var realName: String
    @NSManaged var aboutMe: String
    @NSManaged var pendingBuddies: Array<String>
    @NSManaged var buddies: Array<String>
    @NSManaged var color: String
    @NSManaged var photos: Array<String>
    @NSManaged var profilePicture: PFFile
    @NSManaged var gender: String
    @NSManaged var location: PFGeoPoint
    @NSManaged var locationHidden: Boolean
    @NSManaged var points: Int
    @NSManaged var popPoints: Double
    //@NSManaged var phone: BSPhone
    @NSManaged var birthday: NSDate
    @NSManaged var blocked: Array<String>
    @NSManaged var tags: Array<String>
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
    
}
