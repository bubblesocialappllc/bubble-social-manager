//
//  BSReport.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse

class BSReport: PFObject, PFSubclassing {
   
    @NSManaged var reporter: BSUser
    @NSManaged var reason: String
    @NSManaged var post: BSPost
    @NSManaged var photo: BSPhoto
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
    
    class func parseClassName() -> String {
        return "Reporting"
    }
    
}
