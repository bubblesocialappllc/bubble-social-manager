//
//  BSPost.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse

class BSPost: PFObject, PFSubclassing {
   
    @NSManaged var authorId: String
    @NSManaged var author: BSUser
    @NSManaged var authorUsername: String
    @NSManaged var text: String
    @NSManaged var hashtag: String
    @NSManaged var photo: PFFile
    @NSManaged var commenters: Array<String>
    @NSManaged var comments: Int
    @NSManaged var likers: Array<String>
    @NSManaged var hasLikedBefore: Array<String>
    @NSManaged var likes: Int
    @NSManaged var location: PFGeoPoint
    @NSManaged var locationName: String
    @NSManaged var points: Double
    @NSManaged var profilePic: PFFile
    @NSManaged var reporters: Array<String>
    @NSManaged var mentioned: Array<String>
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
    
    class func parseClassName() -> String {
        return "Post"
    }
    
}
