//
//  BSPhoto.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse

class BSPhoto: PFObject, PFSubclassing {
   
    @NSManaged var user: BSUser
    @NSManaged var photo: PFFile
    @NSManaged var post: String
    @NSManaged var tags: Array<String>
    @NSManaged var comments: Array<String>
    
    override class func initialize() {
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken) {
            self.registerSubclass()
        }
    }
    
    class func parseClassName() -> String {
        return "Photo"
    }
    
}
