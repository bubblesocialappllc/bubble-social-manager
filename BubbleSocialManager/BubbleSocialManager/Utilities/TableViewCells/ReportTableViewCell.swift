//
//  ReportTableViewCell.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ReportTableViewCell: UITableViewCell {
    
    @IBOutlet var photo: PFImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
