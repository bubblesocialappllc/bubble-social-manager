//
//  ReportTableViewController.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/23/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ReportTableViewController: UITableViewController {
    
    var reports = [AnyObject]()
    var skip = 0, limit = 3
    
    var reportType: String? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return reports.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: UIView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 50))
        header.backgroundColor = UIColor.whiteColor()
        
        let report: BSReport = reports[section] as! BSReport
        let photo: BSPhoto = report.photo
        let post: BSPost = report.post
        
        let profilePicture: PFImageView = PFImageView(frame: CGRectMake(8, 2, 45, 45))
        profilePicture.contentMode = .ScaleAspectFill
        profilePicture.clipsToBounds = true
        profilePicture.layer.cornerRadius = profilePicture.frame.height / 2
        
        let username: UILabel = UILabel(frame: CGRectMake(profilePicture.frame.origin.x + (profilePicture.frame.width + 5), 12, header.frame.width - 55, 25))
        
        if photo.user.username != nil {
            profilePicture.file = photo.user.profilePicture
            profilePicture.loadInBackground()
            username.text = photo.user.username
        } else {
           username.text = post.authorUsername
        }
        
        header.addSubview(profilePicture)
        header.addSubview(username)
        
        return header
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reportCell", forIndexPath: indexPath) as! ReportTableViewCell
        
        let report: BSReport = reports[indexPath.section] as! BSReport
        let photo: BSPhoto = report.photo
        let post: BSPost = report.post
        
        cell.photo.file = photo.photo;
        cell.photo.loadInBackground()
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        
    }
    
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
        
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // When we scroll far enough, we should automatically start to fetch for new objects.
        //println("\(scrollView.distanceToBottom())")
        if scrollView.distanceToBottom() < 250 {
            self.configureView()
        }
    }
    
    // MARK: - Helpers
    
    func configureView() {
        // Update the user interface for the detail item.
        if let report: String = self.reportType {
            self.title = reportType
            
            if self.title == "Inappropriate" {
                let inappropriateQuery = BSReport.query()
                inappropriateQuery!.includeKey("post")
                inappropriateQuery!.includeKey("photo.user")
                inappropriateQuery!.whereKey("reason", equalTo: "Inappropriate")
                inappropriateQuery!.skip = skip;
                inappropriateQuery!.limit = limit
                inappropriateQuery!.findObjectsInBackgroundWithBlock{ (objects, error) -> Void in
                    
                    if objects!.count < self.limit {
                        self.skip += objects!.count // We got back less than our limit. Update skip so we don't query the same object
                    } else if objects!.count == self.limit {
                        self.skip += self.limit // There might be more objects in the table. Update the skip value.
                    }
                    
                    self.reports.extend(objects!)
                    self.tableView.reloadData()
                }
            } else if self.title == "Spamming" {
                let spammingQuery = BSReport.query()
                spammingQuery!.includeKey("post")
                spammingQuery!.includeKey("photo.user")
                spammingQuery!.whereKey("reason", equalTo: "Spamming")
                spammingQuery!.skip = skip;
                spammingQuery!.limit = limit
                spammingQuery!.findObjectsInBackgroundWithBlock{ (objects, error) -> Void in
                    
                    if objects!.count < self.limit {
                        self.skip += objects!.count // We got back less than our limit. Update skip so we don't query the same object
                    } else if objects!.count == self.limit {
                        self.skip += self.limit // There might be more objects in the table. Update the skip value.
                    }
                    
                    self.reports.extend(objects!)
                    self.tableView.reloadData()
                }
             } else if self.title == "Other" {
                let otherQuery = BSReport.query()
                otherQuery!.includeKey("post")
                otherQuery!.includeKey("photo.user")
                otherQuery!.whereKey("reason", equalTo: "Other")
                otherQuery!.skip = skip;
                otherQuery!.limit = limit
                otherQuery!.findObjectsInBackgroundWithBlock{ (objects, error) -> Void in
                    
                    if objects!.count < self.limit {
                        self.skip += objects!.count // We got back less than our limit. Update skip so we don't query the same object
                    } else if objects!.count == self.limit {
                        self.skip += self.limit // There might be more objects in the table. Update the skip value.
                    }
                    
                    self.reports.extend(objects!)
                    self.tableView.reloadData()
                }

            } else if self.title == "All" {
                let allQuery = BSReport.query()
                allQuery!.includeKey("post")
                allQuery!.includeKey("photo.user")
                allQuery!.skip = skip;
                allQuery!.limit = limit
                allQuery!.findObjectsInBackgroundWithBlock{ (objects, error) -> Void in
                    
                    if objects!.count < self.limit {
                        self.skip += objects!.count // We got back less than our limit. Update skip so we don't query the same object
                    } else if objects!.count == self.limit {
                        self.skip += self.limit // There might be more objects in the table. Update the skip value.
                    }
                    
                    self.reports.extend(objects!)
                    self.tableView.reloadData()
                }
            }
            
        }
    }
    

}

extension UIScrollView {

    func distanceToBottom() -> CGFloat {
        
        let contentOffset = self.contentOffset
        let contentSize = self.contentSize
        let bounds = self.bounds
        return abs(contentOffset.y - (contentSize.height - bounds.size.height))
        
    }
    
}
