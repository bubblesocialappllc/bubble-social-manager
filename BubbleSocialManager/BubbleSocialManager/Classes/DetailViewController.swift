//
//  DetailViewController.swift
//  BubbleSocialManager
//
//  Created by Chayel Heinsen on 3/22/15.
//  Copyright (c) 2015 Bubble Social App LLC. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {
    
    // Reporting
    var all:Int = 0, inappropriate: Int = 0, spamming: Int = 0, other: Int = 0
    
    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        // If there is a selected row, let's deselect it.
        if let selectedPath = self.tableView.indexPathForSelectedRow() {
            self.tableView.deselectRowAtIndexPath(selectedPath, animated: true)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        
        if self.title == "Reports" {
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "All"
                cell.detailTextLabel?.text = "\(all)"
            case 1:
                cell.textLabel?.text = "Inappropriate"
                cell.detailTextLabel?.text = "\(inappropriate)"
            case 2:
                cell.textLabel?.text = "Spamming"
                cell.detailTextLabel?.text = "\(spamming)"
            case 3:
                cell.textLabel?.text = "Other"
                cell.detailTextLabel?.text = "\(other)"
            default:
                cell.textLabel?.text = ""
                cell.detailTextLabel?.text = ""
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.title == "Reports" {
            self.performSegueWithIdentifier("showReport", sender: nil)
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showReport" {
            
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                
                let reportType = tableView.cellForRowAtIndexPath(indexPath)?.textLabel?.text
                let controller = segue.destinationViewController as! ReportTableViewController
                controller.reportType = reportType
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Helpers
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            self.title = detail as? String
            
            if self.title == "Reports" {
                let allQuery = BSReport.query()
                allQuery?.countObjectsInBackgroundWithBlock({ (number, error) -> Void in
                    self.all = Int(number)
                    self.tableView.reloadData()
                })
                
                let inappropriateQuery = BSReport.query()
                inappropriateQuery!.whereKey("reason", equalTo: "Inappropriate")
                inappropriateQuery?.countObjectsInBackgroundWithBlock { (number, error) -> Void in
                    self.inappropriate = Int(number)
                    self.tableView.reloadData()
                }
                
                let spammingQuery = BSReport.query()
                spammingQuery!.whereKey("reason", equalTo: "Spamming")
                spammingQuery?.countObjectsInBackgroundWithBlock { (number, error) -> Void in
                    self.spamming = Int(number)
                    self.tableView.reloadData()
                }
                
                let otherQuery = BSReport.query()
                otherQuery!.whereKey("reason", equalTo: "Other")
                otherQuery?.countObjectsInBackgroundWithBlock { (number, error) -> Void in
                    self.other = Int(number)
                    self.tableView.reloadData()
                }
            }
            
        }
    }

}

